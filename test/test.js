'use strict';
const expect = require("chai").expect;
require("mocha");
const sole = require("../dist/index.js");
var testSrc = `
    def a = 1;
    def b = 2;
    
    def ret15() {
        return 15;
    }

    def ret15a(a) {
        return 15 + a;
    }
    
    def ret15b() {
        return ret15();
    }

    def ret15c() {
        def c = ret15();
        return c;
    }

    def ret15d() {
        def c = ret15();
        return c;
    }

    def ret15e(a) {
        def c = ret15();
        return c + a;
    }

    def f(c) {
        def d = 5;
        def j = d * (c + a) + b;
        return j;
    }

    def g(c) {
        return c == 1 || a == 2 && b == 2;
    }
    
    def h(c) {
        def l = f(c);
        return 1 + l;
    }
    
    def i() {
        f(1);
        return false;
    }
    
    def w() {
        def c = 1;
        while (c > 0) {
            c = c - 1;
        }
    }

    def y() {
        def c = 4;
        while (c > 1) {
            c = c - 1;
            if (c < 3) return 3;
        }
        return 4;
    }

    def x() {
        y();
        return 5;
    }

    def a() {
        b(); // undefined
    }

    def a2() {
        return 1 == undefinedconst;
    }
`;
let sb = new sole.SoleBuilder();
var binding = new sole.Binding(null);
binding.addConstant("false", false);
binding.addConstant("true", true);
binding.addFunction("log", console.log);
let prog = sb.build(binding, testSrc);
describe("errors", () => {
    let ee = [
        ["a; def x; y", "line 1:0 mismatched input 'a' expecting {'def', 'import'}"],
        ["def x; yaa; yee;", "line 1:7 extraneous input 'yaa' expecting {<EOF>, 'def'}"],
        ["yaa; yee;", "line 1:0 mismatched input 'yaa' expecting {'def', 'import'}"],
    ];
    for (let e of ee) {
        it("Syntax errors", () => {
            let sb = new sole.SoleBuilder();
            var binding = new sole.Binding(null);
            binding.addConstant("false", false);
            binding.addConstant("true", true);
            binding.addFunction("log", console.log);
            /* try {
                 sb.build(binding, <string>e[0])
             } catch (e) {
                 //console.error(e)
                 console.error(e.message)
             }*/
            expect(sb.build.bind(sb, binding, e[0])).to.throw(e[1]);
        });
    }
});
describe('functions', () => {
    let tt = [
        /*  1 function to call
            2 parameters list
            3 expected value
            4 expected error message */
        ["i", [], false],
        ["w", [], null],
        ["g", [1], true],
        ["f", [1], 12],
        ["g", [2], false],
        ["g", [2], false],
        ["f", [1], 12],
        ["h", [1], 13],
        ["ret15", [], 15],
        ["ret15a", [1], 16],
        ["ret15b", [], 15],
        ["ret15c", [], 15],
        ["ret15d", [], 15],
        ["ret15e", [1], 16],
        ["y", [], 3],
        ["x", [], 5],
        ["a", [], null, "Function b not found"],
        ["a2", [], null, "Variable undefinedconst not found"]
    ];
    for (let t of tt) {
        let desc = null;
        if (t[2] == null && t.length == 4) {
            desc = t[0] + "(" + t[1].join(",") + ") should throw \"" + t[3] + "\"";
        }
        else {
            desc = t[0] + "(" + t[1].join(",") + ") should return " + t[2];
        }
        it(desc, () => {
            if (t[2] == null && t.length == 4) {
                expect(prog.call.bind(prog, t[0], t[1])).to.throw(t[3]);
            }
            else {
                const result = prog.call(t[0], t[1]);
                expect(result).to.equal(t[2]);
            }
        });
    }
});