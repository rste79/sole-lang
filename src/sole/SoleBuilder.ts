import { ANTLRInputStream, CommonTokenStream, ParserRuleContext, ANTLRErrorListener, BailErrorStrategy, Parser, RecognitionException, Token, InputMismatchException, DefaultErrorStrategy, Recognizer } from 'antlr4ts';
import { TerminalNode, ErrorNode, ParseTreeWalker } from 'antlr4ts/tree'
import { SoleListener } from './SoleListener';
import { SoleVisitor } from './SoleVisitor';
import { SoleLexer } from './SoleLexer';
import { SoleParser, StatementContext, ExpressionContext, FunctionCallContext, GlobalsDefContext, ImportDefContext, FunctionDefContext, PredicateContext, ProgramContext, StatementBlockContext } from './SoleParser';


export interface ImportLoader {
    loadAndInit(path: string, binding: Binding):any;
}

export class Binding {

    private _parent: Binding;
    private _consts: { [k: string]: any } = {};
    private _variables: { [k: string]: any } = {};
    private _functions: { [k: string]: any } = {};
    
    
    public get parent() : Binding {
        return this._parent;
    }

    constructor(parentBinding: Binding) {
        this._parent = parentBinding;
    }

    addVariable(name: string, value: any): void {
        if (this.hasLocalVariable(name))
            throw new Error("Variable " + name + " already exists");
        this._variables[name] = value;
    }

    setVariable(name: string, value: any): void {
        if (!this.hasVariable(name))
            throw new Error("Variable " + name + " not found");
        if (this._parent && this._parent.hasVariable(name))
            this._parent.setVariable(name, value);
        else
            this._variables[name] = value;
    }

    getVariable(name: string): any {
        if (!this.hasVariable(name))
            throw new Error("Variable " + name + " not found");
        if (this._parent && this._parent.hasVariable(name))
            return this._parent.getVariable(name);
        else
            return this._variables[name];
    }

    hasLocalVariable(name: string): boolean {
        return this._variables.hasOwnProperty(name);
    }

    hasVariable(name: string): boolean {
        return (this._parent && this._parent.hasVariable(name)) || this._variables.hasOwnProperty(name);
    }

    hasConstant(name: string): boolean {
        return (this._parent && this._parent.hasConstant(name)) || this._consts.hasOwnProperty(name);
    }

    addConstant(name: string, value: any): void {
        if (this._consts[name])
            throw new Error("Constant " + name + " already exists");
        this._consts[name] = value;
    }

    getConstant(name: string): any {
        if (!this.hasConstant(name))
            throw new Error("Constant " + name + " not found");
        if (this._parent && this._parent.hasConstant(name))
            return this._parent.getConstant(name);
        else
            return this._consts[name];
    }

    hasFunction(name: string): boolean {
        return (this._parent && this._parent.hasFunction(name)) || this._functions.hasOwnProperty(name);
    }

    addFunction(name: string, f: any): void {
        if (this._functions[name])
            throw new Error("Function " + name + " already exists");
        this._functions[name] = f;
    }

    getFunction(name: string): any {
        if (!this.hasFunction(name))
            throw new Error("Function " + name + " not found");
        if (this._parent && this._parent.hasFunction(name))
            return this._parent.getFunction(name);
        else
            return this._functions[name];
    }

}

export class ASTNode {
    line: number;
    column: number;
    //stop: number;
    
    constructor(line: number, column: number) {
        this.line = line;
        this.column = column;
    }

    eval(binding: Binding): any {
        return null;
    }
}

var callStack: string[] = [];
var returnFlag: boolean = false;
class FunctionCall extends ASTNode {
    private _name: string;
    private _args: ASTNode[];

    constructor(line: number, column: number, name: string, args: ASTNode[]) {
        super(line, column);
        this._name = name;
        this._args = args;
    }

    eval(b: Binding): any {
        var args = [];
        args = args.concat(this._args.map((e) => e.eval(b)));
        var f = b.getFunction(this._name);
        callStack.push(this._name);
        var ret = f.apply(b.parent, args);
        callStack.pop();
        returnFlag = false;
        return ret; 
    }
}

class IdentifierLiteral extends ASTNode {
    private _id: string;

    constructor(line: number, column: number, id) {
        super(line, column);
        this._id = id;
    }

    eval(b: Binding): any {
        var ret = this.eval2(b);
        return ret;
    }

    eval2(b: Binding): any {
        if (b.hasConstant(this._id))
            return b.getConstant(this._id);
        else
            return b.getVariable(this._id);
    }
}

class NumberLiteral extends ASTNode {
    private _value: number;

    constructor(line: number, column: number, value: string) {
        super(line, column);
        this._value = parseFloat(value);
    }

    eval(b: Binding): any { return this._value; }
}

class StringLiteral extends ASTNode {
    _value: string;

    constructor(line: number, column: number, value: string) {
        super(line, column);
        this._value = value.substring(1, value.length - 1).replace(/\"/g, '"');
    }

    eval(b: Binding): any { return this._value; }
}

class Subscript extends ASTNode {
    private _expr: ASTNode;
    private _idx: ASTNode;

    constructor(line: number, column: number, expr: ASTNode, idx: ASTNode) {
        super(line, column);
        this._expr = expr;
        this._idx = idx;
    }

    eval(b: Binding): any {
        var e = this._expr.eval(b);
        if (e instanceof Array)
            return e[this._idx.eval(b)];
        else
            throw new Error(this.line + ":" + this.column + " Cannot apply subscript to " + typeof (e));
    }
}

class Statement extends ASTNode {
}

class DefStatement extends Statement {

    private _name: string;
    private _expr: ASTNode;

    constructor(line: number, column: number, name: string, expr: ASTNode) {
        super(line, column);
        this._name = name;
        this._expr = expr;
    }

    eval(b: Binding): any {
        b.addVariable(this._name, this._expr ? this._expr.eval(b) : null);
        return null;
    }
}

class IfStatement extends Statement {
    private _p: ASTNode;
    private _t: Statement;
    private _e: Statement;

    constructor(line: number, column: number, p: ASTNode, t: Statement, e: Statement) {
        super(line, column);
        this._p = p;
        this._t = t;
        this._e = e;
    }

    eval(b: Binding): any {
        var pv = this._p.eval(b);
        if (typeof pv == "boolean") {
            if (pv) {
                return this._t.eval(b);
            } else {
                if (this._e != null)
                    return this._e.eval(b);
            }
            return null;
        } else {
            throw new Error("if statement expects a predicate");
        }
    }
}

class AssignmentStatement extends Statement {
    private _name: string;
    private _expr: ASTNode;

    constructor(line: number, column: number, name: string, expr: ASTNode) {
        super(line, column);
        this._name = name;
        this._expr = expr;
    }

    eval(b: Binding): any {
        b.setVariable(this._name, this._expr.eval(b));
        return null;
    }
}

class DoWhileStatement extends Statement {
    private _p: ASTNode;
    private _stmt: Statement;

    constructor(line: number, column: number, p: ASTNode, stmt: Statement) {
        super(line, column);
        this._p = p;
        this._stmt = stmt;
    }

    eval(b: Binding): any {
        var c: boolean = this._p.eval(b);
        do {
            var r = this._stmt.eval(b);
            if (r != null)
                return r;
        } while (this._p.eval(b));
        return null;
    }
}

class WhileStatement extends Statement {
    private _p: ASTNode;
    private _stmt: Statement;

    constructor(line: number, column: number, p: ASTNode, stmt: Statement) {
        super(line, column);
        this._p = p;
        this._stmt = stmt;
    }

    eval(b: Binding): any {
        var c: boolean = this._p.eval(b);
        while (c == true) {
            var r = this._stmt.eval(b);
            if (returnFlag) return r;
            // if (r != null)
            //     return r;
            c = this._p.eval(b);
        }
        return null;
    }
}

class ReturnStatement extends Statement {
    private _expr: ASTNode;

    constructor(line: number, column: number, expr: ASTNode) {
        super(line, column);
        this._expr = expr;
    }

    eval(b: Binding): any {
        var ret = this._expr.eval(b);
        returnFlag = true;
        return ret;
    }
}

class StatementsBlock extends Statement {
    private _statements: Statement[];

    constructor(line: number, column: number, statements: Statement[]) {
        super(line, column);
        this._statements = statements;
    }

    eval(b: Binding): any {
        var blockB = new Binding(b);
        for (var i = 0; i < this._statements.length; i++) {
            var s = this._statements[i];
            var ret = s.eval(blockB);
            if (s instanceof ReturnStatement) {
                return ret;
            }
            if (returnFlag) return ret;
        }
        return null;
    }
}

class Predicate extends ASTNode {
    eval(binding: Binding): boolean {
        return false;
    };
}

class And extends Predicate {
    private _p1: ASTNode;
    private _p2: ASTNode;

    constructor(line: number, column: number, p1: ASTNode, p2: ASTNode) {
        super(line, column);
        this._p1 = p1;
        this._p2 = p2;
    }

    eval(b: Binding): boolean {
        return this._p1.eval(b) && this._p2.eval(b);
    }
}

class Or extends Predicate {
    private _p1: ASTNode;
    private _p2: ASTNode;

    constructor(line: number, column: number, p1: ASTNode, p2: ASTNode) {
        super(line, column);
        this._p1 = p1;
        this._p2 = p2;
    }

    eval(b: Binding): boolean {
        return this._p1.eval(b) || this._p2.eval(b);
    }
}

class Not extends Predicate {
    private _p: ASTNode;

    constructor(line: number, column: number, p: ASTNode) {
        super(line, column);
        this._p = p;
    }

    eval(b: Binding): boolean {
        return !this._p.eval(b);
    }
}

class Comp extends Predicate {
    private _l: ASTNode;
    private _r: ASTNode;
    private _op: string;
    constructor(line: number, column: number, l: ASTNode, r: ASTNode, op: string) {
        super(line, column);
        this._l = l;
        this._r = r;
        this._op = op;
    }

    eval(b: Binding): boolean {
        var l = this._l.eval(b);
        var r = this._r.eval(b);
        switch (this._op) {
            case '<': return l < r;
            case '<=': return l <= r;
            case '>': return l > r;
            case '>=': return l >= r;
            case '==': return l == r;
            case '!=': return l != r;
            case 'like': return typeof l == "string" && typeof r == "string" && (new RegExp(r)).test(l);
            default: throw new Error("Operator " + this._op + " unexpected");
        }
    }
}

class Expr extends ASTNode {
}

class UnaryOp extends Expr {
    private _r: ASTNode;
    private _op: string;

    constructor(line: number, column: number, r: ASTNode, op: string) {
        super(line, column);
        this._r = r;
        this._op = op;
    }

    eval(b: Binding): any {
        var r = this._r.eval(b);
        switch (this._op) {
            case '-': return - r;
            default: throw new Error("Operator " + this._op + " unexpected");
        }
    }
}

class BinaryOp extends Expr {
    private _l: ASTNode;
    private _r: ASTNode;
    private _op: string;
    constructor(line: number, column: number, l: ASTNode, r: ASTNode, op: string) {
        super(line, column);
        this._l = l;
        this._r = r;
        this._op = op;
    }

    eval(b: Binding): any {
        var l = this._l.eval(b);
        var r = this._r.eval(b);
        switch (this._op) {
            case '*': return l * r;
            case '/': return l / r;
            case '+': return l + r;
            case '-': return l - r;
            case '%': return l % r;
            case '?:': return l == null ? r : l;
            default: throw new Error("Operator " + this._op + " unexpected");
        }
    }
}

class Iff extends Expr {
    private _p: ASTNode;
    private _l: ASTNode;
    private _r: ASTNode;

    constructor(line: number, column: number, p: ASTNode, l: ASTNode, r: ASTNode) {
        super(line, column);
        this._p = p;
        this._l = l;
        this._r = r;
    }

    eval(b: Binding): any {
        var l = this._l.eval(b);
        var r = this._r.eval(b);
        return this._p.eval(b) ? l : r;
    }
}

class ImportDef extends ASTNode {
    private _path: string;

    constructor(line: number, column: number, path: string) {
        super(line, column);
        this._path = path;
    }
}

class GlobalDef extends ASTNode {
    private _native: boolean;
    private _name: string;
    private _value: ASTNode;

    constructor(line: number, column: number, native: boolean, name: string, value: ASTNode) {
        super(line, column);
        this._native = native;
        this._name = name;
        this._value = value;
    }

    /*eval(b: Binding): any {
        b.addVariable(this._name, this._value.eval(b));
        return null;
    }*/
}

class FunctionDef extends ASTNode {
    private _native: boolean;
    private _name: string;
    private _statements: StatementsBlock;
    private _params: string[];

    constructor(line: number, column: number, native: boolean, name: string, params: string[], statements: StatementsBlock) {
        super(line, column);
        this._native = native;
        this._name = name;
        this._params = params;
        this._statements = statements;
    }


    public get name(): string {
        return this._name;
    }
    
    apply(ctx: Binding, args: any[]): any {
        var b = new Binding(ctx);
        if (this._params.length != args.length) {
            throw new Error("Function " + this._name + ": parameter count mismatch: expected " + this._params.length + " parameters, passed " + args.length + " parameters");
        }
        for (var i = 0; i < this._params.length; i++) {
            b.addVariable(this._params[i], args[i]);
        }
        return this._statements.eval(b);
    }

}


function isTerminalNode(o: any): boolean {
    return o.hasOwnProperty("_symbol");
}

function isFunctionCallContext(o: any): boolean {
    return o.constructor.name == "FunctionCallContext";
}

function isStatementBlockContext(o: any): boolean {
    return o.constructor.name == "StatementBlockContext";
}

export class SoleBuilder implements SoleListener {

    private _importLoader: ImportLoader;

    private _stack: ASTNode[];
    private _imports: ImportDef[];
    private _globals: GlobalDef[];
    private _functions: FunctionDef[];
    //private _nativeBinding: Binding;
    private _binding: Binding;

    public set importLoader(il: ImportLoader) {
        this._importLoader = il;
    }
    /*        constructor() {
                super()
            }*/

    enterStatement(context: StatementContext): void { }

    /*
        statement
        : '{' statement* '}'
        | 'def' Identifier ('=' (expression | predicate))? ';'
        | Identifier '=' expression ';'
        | functionCall ';'
        | 'if' '(' predicate ')' statement ('else' statement)?
    //| 'for' '(' forControl ')' statement
        | 'while' '(' predicate ')' statement
        | 'do' statement 'while' '(' predicate ')' ';'
    //| 'switch' '(' expression ')' '{' switchBlockStatementGroup* switchLabel* '}'
        | 'return' (expression | predicate)? ';'
        | ';'
        ;
        */
    exitStatement(context: StatementContext): void {
        var c1 = context.children[0];
        if (isTerminalNode(c1)) {
            switch ((<TerminalNode>c1).symbol.type) {
                case SoleLexer.DEF:
                    this._push(new DefStatement(context.start.line, context.start.charPositionInLine, context.children[1].text, context.childCount == 3 ? null : this._pop(0)));
                    break;
                case SoleLexer.Identifier:
                    this._push(new AssignmentStatement(context.start.line, context.start.charPositionInLine, c1.text, this._pop(0)));
                    break;
                case SoleLexer.IF:
                    if (context.childCount == 7)
                        this._push(new IfStatement(context.start.line, context.start.charPositionInLine, this._pop(2), this._pop(1), this._pop(0)));
                    else
                        this._push(new IfStatement(context.start.line, context.start.charPositionInLine, this._pop(1), this._pop(0), null));
                    break;
                case SoleLexer.WHILE:
                    this._push(new WhileStatement(context.start.line, context.start.charPositionInLine, this._pop(1), this._pop(0)));
                    break;
                case SoleLexer.DO:
                    this._push(new DoWhileStatement(context.start.line, context.start.charPositionInLine, this._pop(1), this._pop(0)));
                    break;
                case SoleLexer.RETURN:
                    this._push(new ReturnStatement(context.start.line, context.start.charPositionInLine, this._pop(0)));
                    break;
                default:
                    throw new Error("Unexpected symbol " + (<TerminalNode>c1).symbol.text);
            }
            return;
        } else if (isStatementBlockContext(c1) || isFunctionCallContext(c1)) {
            // this._push(this._pop(0));
        } else {
            //console.log(["Unexpected token", context.children, context.children[0].text]);
            throw new Error("Unexpected token " + c1 + " at " + context.start.line + "," + context.start.charPositionInLine);
        }
    }


    enterExpression(context: ExpressionContext): void { }

    /*
        expression
        : '(' expression ')'
        | '-' expression
        | expression ('*'|'/'|'%') expression
        | expression ('+'|'-') expression
        | 'iff' '(' predicate ',' expression ',' expression ')'
        | expression '?:' expression
        | expression '[' (Number | Identifier | StringLiteral) ']'
        | functionCall
        | Identifier
        | Number
        | StringLiteral
        ;
        */
    exitExpression(context: ExpressionContext): void {
        var cc = context.children[0].text;
        if (context.childCount == 3) {
            if (cc != "(") this._push(new BinaryOp(context.start.line, context.start.charPositionInLine, this._pop(1), this._pop(0), context.children[1].text));
        } else if (context.childCount == 2 && isTerminalNode(context.children[0])) {
            this._push(new UnaryOp(context.start.line, context.start.charPositionInLine, this._pop(0), context.children[0].text));
        } else if (context.childCount == 4 && context.children[1].text == "[") {
            this._push(new Subscript(context.start.line, context.start.charPositionInLine, this._pop(1), this._pop(0)));
        } else if (context.childCount == 8 && context.children[0].text == "iff") {
            this._push(new Iff(context.start.line, context.start.charPositionInLine, this._pop(2), this._pop(1), this._pop(0)));
        } else if (context.childCount == 1) {
            var c1 = <TerminalNode>context.children[0];

            if (isTerminalNode(c1)) {
                switch (c1.symbol.type) {
                    case SoleLexer.Identifier:
                        this._push(new IdentifierLiteral(context.start.line, context.start.charPositionInLine, c1.text));
                        break;
                    case SoleLexer.Number:
                        this._push(new NumberLiteral(context.start.line, context.start.charPositionInLine, c1.text));
                        break;
                    case SoleLexer.StringLiteral:
                        this._push(new StringLiteral(context.start.line, context.start.charPositionInLine, c1.text));
                        break;
                }
            }
        } else {
            throw new Error("Unidentified expression " + context.toString());
        }
    }


    enterFunctionCall(context: FunctionCallContext): void {
    }


    exitFunctionCall(context: FunctionCallContext): void {
        var args: ASTNode[] = [];
        var name = context.children[0].text;
        //var i = Math.round((context.children.length - 3) / 2.0) - 1;
        var count = 0;
        var i = 2;

        while (context.children[i].text != ')') {
            if (context.children[i].text != ',') count++;
            i++;
        }

        while (count > 0) {
            args.push(this._pop(count - 1));
            count--;
        };

        this._push(new FunctionCall(context.start.line, context.start.charPositionInLine, name, args));
    }


    enterStatementBlock(context: StatementBlockContext): void { }

    exitStatementBlock(context: StatementBlockContext): void {
        var stmts: ASTNode[] = [];
        var i = context.children.length - 3;
        
        while (i >= 0) {
            stmts.push(this._pop(i));
            i--;
        };
        this._push(new StatementsBlock(context.start.line, context.start.charPositionInLine, stmts));
    }


    enterPredicate(context: PredicateContext): void {
    }

    /*
        predicate
        : '(' predicate ')'
        | '!' predicate
        | predicate '&&' predicate
        | predicate '||' predicate
        | expression ('<=' | '>=' | '>' | '<') expression
        | expression ('==' | '!=' | 'like') expression
        | functionCall
        | Identifier 
        ;
    */
    exitPredicate(context: PredicateContext): void {
        if (context.childCount == 3) {
            var c1 = <TerminalNode>context.children[0];
            var c2 = <TerminalNode>context.children[1];
            if (isTerminalNode(c2)) {
                if (c2.symbol.type == SoleLexer.AND) {
                    this._push(new And(context.start.line, context.start.charPositionInLine, this._pop(1), this._pop(0)));
                } else if (c2.symbol.type == SoleLexer.OR) {
                    this._push(new Or(context.start.line, context.start.charPositionInLine, this._pop(1), this._pop(0)));
                } else {
                    this._push(new Comp(context.start.line, context.start.charPositionInLine, this._pop(1), this._pop(0), c2.symbol.text));
                }
            }
        } else if (context.childCount == 2) {
            var c1 = <TerminalNode>context.children[0];
            if (isTerminalNode(c1)) {
                if (c1.symbol.type == SoleLexer.BANG) {
                    this._push(new Not(context.start.line, context.start.charPositionInLine, this._pop(0)));
                }
            }
        } else if (context.childCount == 1) {
            var c1 = <TerminalNode>context.children[0];
            if (isTerminalNode(c1)) {
                this._push(new IdentifierLiteral(context.start.line, context.start.charPositionInLine, c1.text));
            } else if (isFunctionCallContext(c1)) {
                // this._push(this._pop(0));
            } else
                throw new Error("Unidentified expression " + context.children);
        } else
            throw new Error("Unidentified expression " + context.children);

    }

    private _push(n: ASTNode): void {
        if (!n) throw new Error("Cannot push null ASTNode");
        this._stack.push(n);
    }

    private _pop(pos: number): ASTNode {
        var idx = this._stack.length - pos - 1;
        if (idx < 0) {
            throw new Error("Illegal pop index " + pos + ", stack length is " + this._stack.length);
        }
        var ret = this._stack[idx];
        this._stack = this._stack.slice(0, idx).concat(this._stack.slice(idx + 1));
        return ret;
    }

    /*
    functionDef
        : 'def' 'native' Identifier '(' (Identifier (',' Identifier)*)? ')' '{' '}'
        | 'def' Identifier '(' (Identifier (',' Identifier)*)? ')' statementBlock
        ;
    */
    enterFunctionDef(ctx: FunctionDefContext): void {
    }

    exitFunctionDef(ctx: FunctionDefContext): void {
        var c1 = <TerminalNode>ctx.children[1];
        var native = isTerminalNode(c1) && c1.symbol.type == SoleLexer.NATIVE;
        var idx = native ? 2 : 1;
        var name = ctx.children[idx].text;
        if (native && !this._binding.hasFunction(name))
            throw new Error("Missing function declaration " + name + " in native binding");
        else if (this._binding.hasFunction(name))
            throw new Error("Duplicate function name " + name);
        idx += 2; // now at first param or ')'
        var params: string[] = [];
        while (ctx.children[idx].text != ')') {
            if (ctx.children[idx].text != ',')
                params.push(ctx.children[idx].text);
            idx++
        }
        var st: StatementsBlock;
        if (!native) {
            st = <StatementsBlock>this._pop(0);
            this._binding.addFunction(name, new FunctionDef(ctx.start.line, ctx.start.charPositionInLine, native, name, params, st));
        }
    }

    /*
    globalsDef
        : 'def' 'native' Identifier ';'
        | 'def' Identifier ('=' (Number | StringLiteral))? ';'
        ;
    */
    enterGlobalsDef(ctx: GlobalsDefContext): void {
        var c1 = <TerminalNode>ctx.children[1];
        var native = isTerminalNode(c1) && c1.symbol.type == SoleLexer.NATIVE;
        var idx = native ? 2 : 1;
        var name = ctx.children[idx].text;
        var value: ASTNode = null;
        if (!native && ctx.childCount > 4) {
            var c2 = <TerminalNode>ctx.children[3];

            if (isTerminalNode(c2)) {
                switch (c2.symbol.type) {
                    case SoleLexer.Number:
                        value = new NumberLiteral(ctx.start.line, ctx.start.charPositionInLine, c2.text);
                        break;
                    case SoleLexer.StringLiteral:
                        value = new StringLiteral(ctx.start.line, ctx.start.charPositionInLine, c2.text);
                        break;
                    default:
                        throw new Error("Unexpected symbol type in global variable definition");
                }
            } else {
                throw new Error("Unexpected token type in global variable definition");
            }
        }
        if (native && !this._binding.hasVariable(name)) throw new Error("Missing constant declaration " + name + " in native binding");
        if (!native) this._binding.addVariable(name, value ? value.eval(null) : null);
        this._globals.push(new GlobalDef(ctx.start.line, ctx.start.charPositionInLine, native, name, value));
    }

    exitGlobalsDef(ctx: GlobalsDefContext): void {
    }

    /*
    importDef
        : 'import' StringLiteral ';'
        ;
    */
    enterImportDef(ctx: ImportDefContext): void {
        var p: string;
        var c1 = <TerminalNode>ctx.children[1];
        if (isTerminalNode(c1) && c1.symbol.type == SoleLexer.StringLiteral) {
            p = new StringLiteral(ctx.start.line, ctx.start.charPositionInLine, c1.text).eval(null);
            if (!/^[A-Za-z0-9][A-Za-z0-9/]+[A-Za-z0-9]$/g.test(p)) throw new Error("Invalid path in import statement")
            this._imports.push(new ImportDef(ctx.start.line, ctx.start.charPositionInLine, p));
            this._importLoader.loadAndInit(p, this._binding);
        }
    }

    exitImportDef(ctx: ImportDefContext): void {

    }

    /*
    program
        : importDef* (globalsDef | functionDef)+
        ;
    */
    enterProgram(ctx: ProgramContext): void {

    }

    exitProgram(ctx: ProgramContext): void {

    }

    enterEveryRule(context: ParserRuleContext): void { }


    exitEveryRule(context: ParserRuleContext): void { }


    visitTerminal(node: TerminalNode): void { }


    visitErrorNode(node: ErrorNode): void { }

    build(nativeBinding: Binding, source: string): IProgram {
        this._stack = [];
        this._imports = [];
        this._globals = [];
        this._functions = [];
        this._binding = new Binding(nativeBinding);
        var ss = new ANTLRInputStream(source);
        var lexer = new SoleLexer(ss);
        var tSource = new CommonTokenStream(lexer);
        var parser = new SoleParser(tSource);
        parser.addErrorListener(new SoleErrorListener());
        //parser.errorHandler = new SoleErrorStrategy();
        parser.buildParseTree = true;
        var pt = parser.program();
        ParseTreeWalker.DEFAULT.walk(this, pt);
        if (parser.numberOfSyntaxErrors > 0) throw new Error("Syntax errors found")
        var prog = new Program(this._binding, this._imports, this._globals, this._functions);
        return prog;
    }
}

class SoleErrorListener implements ANTLRErrorListener<Symbol> {
    syntaxError(recognizer: Recognizer<Symbol, any>, offendingSymbol: any, line: number, charPositionInLine: number, msg: string, e: RecognitionException): void {
        throw new Error(`line ${line}:${charPositionInLine} ${msg}`);
    }

}

class SoleErrorStrategy extends DefaultErrorStrategy {
    recover(recognizer: Parser, e: RecognitionException): void {
        for (let context = recognizer.context; context; context = context.parent) {
            context.exception = e;
        }
        throw e;
    }
    /** Make sure we don't attempt to recover inline; if the parser
     *  successfully recovers, it won't throw an exception.
     */
    recoverInline(recognizer: Parser): Token {
        let e = new InputMismatchException(recognizer);
        for (let context = recognizer.context; context; context = context.parent) {
            context.exception = e;
        }
        throw e;
    }
    /** Make sure we don't attempt to recover from problems in subrules. */
    sync(recognizer: Parser): void {

    }
}

export interface IProgram {
    call(name: string, args: any): any;
}

class Program extends ASTNode implements IProgram  {
    private _imports: ImportDef[];
    private _globals: GlobalDef[];
    private _functions: FunctionDef[];
    private _binding: Binding;

    constructor(binding: Binding, imports: ImportDef[], globals: GlobalDef[], functions: FunctionDef[]) {
        super(0, 0);
        this._binding = binding;
        this._imports = imports;
        this._globals = globals;
        this._functions = functions;
    }

    call(name: string, args: any): any {
        returnFlag = false;
        var _args = args == null ? [] : args;
        var f = this._binding.getFunction(name);
        return f.apply(this._binding, _args)
    }
}
