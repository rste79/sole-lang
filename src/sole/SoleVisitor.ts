// Generated from ./src/sole/Sole.g4 by ANTLR 4.6-SNAPSHOT


import { ParseTreeVisitor } from 'antlr4ts/tree/ParseTreeVisitor';

import { ExpressionContext } from './SoleParser';
import { PredicateContext } from './SoleParser';
import { FunctionCallContext } from './SoleParser';
import { FunctionDefContext } from './SoleParser';
import { GlobalsDefContext } from './SoleParser';
import { StatementBlockContext } from './SoleParser';
import { StatementContext } from './SoleParser';
import { ImportDefContext } from './SoleParser';
import { ProgramContext } from './SoleParser';


/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by `SoleParser`.
 *
 * @param <Result> The return type of the visit operation. Use `void` for
 * operations with no return type.
 */
export interface SoleVisitor<Result> extends ParseTreeVisitor<Result> {
	/**
	 * Visit a parse tree produced by `SoleParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitExpression?: (ctx: ExpressionContext) => Result;

	/**
	 * Visit a parse tree produced by `SoleParser.predicate`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPredicate?: (ctx: PredicateContext) => Result;

	/**
	 * Visit a parse tree produced by `SoleParser.functionCall`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFunctionCall?: (ctx: FunctionCallContext) => Result;

	/**
	 * Visit a parse tree produced by `SoleParser.functionDef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFunctionDef?: (ctx: FunctionDefContext) => Result;

	/**
	 * Visit a parse tree produced by `SoleParser.globalsDef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGlobalsDef?: (ctx: GlobalsDefContext) => Result;

	/**
	 * Visit a parse tree produced by `SoleParser.statementBlock`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitStatementBlock?: (ctx: StatementBlockContext) => Result;

	/**
	 * Visit a parse tree produced by `SoleParser.statement`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitStatement?: (ctx: StatementContext) => Result;

	/**
	 * Visit a parse tree produced by `SoleParser.importDef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitImportDef?: (ctx: ImportDefContext) => Result;

	/**
	 * Visit a parse tree produced by `SoleParser.program`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitProgram?: (ctx: ProgramContext) => Result;
}

