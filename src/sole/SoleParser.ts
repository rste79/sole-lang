// Generated from ./src/sole/Sole.g4 by ANTLR 4.6-SNAPSHOT


import { ATN } from 'antlr4ts/atn/ATN';
import { ATNDeserializer } from 'antlr4ts/atn/ATNDeserializer';
import { FailedPredicateException } from 'antlr4ts/FailedPredicateException';
import { NotNull } from 'antlr4ts/Decorators';
import { NoViableAltException } from 'antlr4ts/NoViableAltException';
import { Override } from 'antlr4ts/Decorators';
import { Parser } from 'antlr4ts/Parser';
import { ParserRuleContext } from 'antlr4ts/ParserRuleContext';
import { ParserATNSimulator } from 'antlr4ts/atn/ParserATNSimulator';
import { ParseTreeListener } from 'antlr4ts/tree/ParseTreeListener';
import { ParseTreeVisitor } from 'antlr4ts/tree/ParseTreeVisitor';
import { RecognitionException } from 'antlr4ts/RecognitionException';
import { RuleContext } from 'antlr4ts/RuleContext';
import { RuleVersion } from 'antlr4ts/RuleVersion';
import { TerminalNode } from 'antlr4ts/tree/TerminalNode';
import { Token } from 'antlr4ts/Token';
import { TokenStream } from 'antlr4ts/TokenStream';
import { Vocabulary } from 'antlr4ts/Vocabulary';
import { VocabularyImpl } from 'antlr4ts/VocabularyImpl';

import * as Utils from 'antlr4ts/misc/Utils';

import { SoleListener } from './SoleListener';
import { SoleVisitor } from './SoleVisitor';


export class SoleParser extends Parser {
	public static readonly T__0=1;
	public static readonly Number=2;
	public static readonly StringLiteral=3;
	public static readonly IF=4;
	public static readonly DEF=5;
	public static readonly ELSE=6;
	public static readonly WHILE=7;
	public static readonly DO=8;
	public static readonly LIKE=9;
	public static readonly RETURN=10;
	public static readonly IMPORT=11;
	public static readonly NATIVE=12;
	public static readonly LPAREN=13;
	public static readonly RPAREN=14;
	public static readonly LBRACE=15;
	public static readonly RBRACE=16;
	public static readonly LBRACK=17;
	public static readonly RBRACK=18;
	public static readonly SEMI=19;
	public static readonly COMMA=20;
	public static readonly DOT=21;
	public static readonly ASSIGN=22;
	public static readonly GT=23;
	public static readonly LT=24;
	public static readonly BANG=25;
	public static readonly TILDE=26;
	public static readonly QUESTION=27;
	public static readonly COLON=28;
	public static readonly ELVIS=29;
	public static readonly EQUAL=30;
	public static readonly LE=31;
	public static readonly GE=32;
	public static readonly NOTEQUAL=33;
	public static readonly AND=34;
	public static readonly OR=35;
	public static readonly INC=36;
	public static readonly DEC=37;
	public static readonly ADD=38;
	public static readonly SUB=39;
	public static readonly MUL=40;
	public static readonly DIV=41;
	public static readonly BITAND=42;
	public static readonly BITOR=43;
	public static readonly CARET=44;
	public static readonly MOD=45;
	public static readonly WS=46;
	public static readonly COMMENT=47;
	public static readonly LINE_COMMENT=48;
	public static readonly Identifier=49;
	public static readonly RULE_expression = 0;
	public static readonly RULE_predicate = 1;
	public static readonly RULE_functionCall = 2;
	public static readonly RULE_functionDef = 3;
	public static readonly RULE_globalsDef = 4;
	public static readonly RULE_statementBlock = 5;
	public static readonly RULE_statement = 6;
	public static readonly RULE_importDef = 7;
	public static readonly RULE_program = 8;
	public static readonly ruleNames: string[] = [
		"expression", "predicate", "functionCall", "functionDef", "globalsDef", 
		"statementBlock", "statement", "importDef", "program"
	];

	private static readonly _LITERAL_NAMES: (string | undefined)[] = [
		undefined, "'iff'", undefined, undefined, "'if'", "'def'", "'else'", "'while'", 
		"'do'", "'like'", "'return'", "'import'", "'native'", "'('", "')'", "'{'", 
		"'}'", "'['", "']'", "';'", "','", "'.'", "'='", "'>'", "'<'", "'!'", 
		"'~'", "'?'", "':'", "'?:'", "'=='", "'<='", "'>='", "'!='", "'&&'", "'||'", 
		"'++'", "'--'", "'+'", "'-'", "'*'", "'/'", "'&'", "'|'", "'^'", "'%'"
	];
	private static readonly _SYMBOLIC_NAMES: (string | undefined)[] = [
		undefined, undefined, "Number", "StringLiteral", "IF", "DEF", "ELSE", 
		"WHILE", "DO", "LIKE", "RETURN", "IMPORT", "NATIVE", "LPAREN", "RPAREN", 
		"LBRACE", "RBRACE", "LBRACK", "RBRACK", "SEMI", "COMMA", "DOT", "ASSIGN", 
		"GT", "LT", "BANG", "TILDE", "QUESTION", "COLON", "ELVIS", "EQUAL", "LE", 
		"GE", "NOTEQUAL", "AND", "OR", "INC", "DEC", "ADD", "SUB", "MUL", "DIV", 
		"BITAND", "BITOR", "CARET", "MOD", "WS", "COMMENT", "LINE_COMMENT", "Identifier"
	];
	public static readonly VOCABULARY: Vocabulary = new VocabularyImpl(SoleParser._LITERAL_NAMES, SoleParser._SYMBOLIC_NAMES, []);

	@Override
	@NotNull
	public get vocabulary(): Vocabulary {
		return SoleParser.VOCABULARY;
	}

	@Override
	public get grammarFileName(): string { return "Sole.g4"; }

	@Override
	public get ruleNames(): string[] { return SoleParser.ruleNames; }

	@Override
	public get serializedATN(): string { return SoleParser._serializedATN; }

	constructor(input: TokenStream) {
		super(input);
		this._interp = new ParserATNSimulator(SoleParser._ATN, this);
	}
	public expression(): ExpressionContext;
	public expression(_p: number): ExpressionContext;
	@RuleVersion(0)
	public expression(_p?: number): ExpressionContext {
		if (_p === undefined) {
			_p = 0;
		}

		let _parentctx: ParserRuleContext = this._ctx;
		let _parentState: number = this.state;
		let _localctx: ExpressionContext = new ExpressionContext(this._ctx, _parentState);
		let _prevctx: ExpressionContext = _localctx;
		let _startState: number = 0;
		this.enterRecursionRule(_localctx, 0, SoleParser.RULE_expression, _p);
		let _la: number;
		try {
			let _alt: number;
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 38;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input,0,this._ctx) ) {
			case 1:
				{
				this.state = 19;
				this.match(SoleParser.LPAREN);
				this.state = 20;
				this.expression(0);
				this.state = 21;
				this.match(SoleParser.RPAREN);
				}
				break;

			case 2:
				{
				this.state = 23;
				this.match(SoleParser.SUB);
				this.state = 24;
				this.expression(10);
				}
				break;

			case 3:
				{
				this.state = 25;
				this.match(SoleParser.T__0);
				this.state = 26;
				this.match(SoleParser.LPAREN);
				this.state = 27;
				this.predicate(0);
				this.state = 28;
				this.match(SoleParser.COMMA);
				this.state = 29;
				this.expression(0);
				this.state = 30;
				this.match(SoleParser.COMMA);
				this.state = 31;
				this.expression(0);
				this.state = 32;
				this.match(SoleParser.RPAREN);
				}
				break;

			case 4:
				{
				this.state = 34;
				this.functionCall();
				}
				break;

			case 5:
				{
				this.state = 35;
				this.match(SoleParser.Identifier);
				}
				break;

			case 6:
				{
				this.state = 36;
				this.match(SoleParser.Number);
				}
				break;

			case 7:
				{
				this.state = 37;
				this.match(SoleParser.StringLiteral);
				}
				break;
			}
			this._ctx._stop = this._input.tryLT(-1);
			this.state = 56;
			this._errHandler.sync(this);
			_alt = this.interpreter.adaptivePredict(this._input,2,this._ctx);
			while ( _alt!==2 && _alt!==ATN.INVALID_ALT_NUMBER ) {
				if ( _alt===1 ) {
					if ( this._parseListeners!=null ) this.triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					this.state = 54;
					this._errHandler.sync(this);
					switch ( this.interpreter.adaptivePredict(this._input,1,this._ctx) ) {
					case 1:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, SoleParser.RULE_expression);
						this.state = 40;
						if (!(this.precpred(this._ctx, 9))) throw new FailedPredicateException(this, "this.precpred(this._ctx, 9)");
						this.state = 41;
						_la = this._input.LA(1);
						if ( !(((((_la - 40)) & ~0x1F) === 0 && ((1 << (_la - 40)) & ((1 << (SoleParser.MUL - 40)) | (1 << (SoleParser.DIV - 40)) | (1 << (SoleParser.MOD - 40)))) !== 0)) ) {
						this._errHandler.recoverInline(this);
						} else {
							if (this._input.LA(1) === Token.EOF) {
								this.matchedEOF = true;
							}

							this._errHandler.reportMatch(this);
							this.consume();
						}
						this.state = 42;
						this.expression(10);
						}
						break;

					case 2:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, SoleParser.RULE_expression);
						this.state = 43;
						if (!(this.precpred(this._ctx, 8))) throw new FailedPredicateException(this, "this.precpred(this._ctx, 8)");
						this.state = 44;
						_la = this._input.LA(1);
						if ( !(_la===SoleParser.ADD || _la===SoleParser.SUB) ) {
						this._errHandler.recoverInline(this);
						} else {
							if (this._input.LA(1) === Token.EOF) {
								this.matchedEOF = true;
							}

							this._errHandler.reportMatch(this);
							this.consume();
						}
						this.state = 45;
						this.expression(9);
						}
						break;

					case 3:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, SoleParser.RULE_expression);
						this.state = 46;
						if (!(this.precpred(this._ctx, 6))) throw new FailedPredicateException(this, "this.precpred(this._ctx, 6)");
						this.state = 47;
						this.match(SoleParser.ELVIS);
						this.state = 48;
						this.expression(7);
						}
						break;

					case 4:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, SoleParser.RULE_expression);
						this.state = 49;
						if (!(this.precpred(this._ctx, 5))) throw new FailedPredicateException(this, "this.precpred(this._ctx, 5)");
						this.state = 50;
						this.match(SoleParser.LBRACK);
						this.state = 51;
						this.expression(0);
						this.state = 52;
						this.match(SoleParser.RBRACK);
						}
						break;
					}
					} 
				}
				this.state = 58;
				this._errHandler.sync(this);
				_alt = this.interpreter.adaptivePredict(this._input,2,this._ctx);
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public predicate(): PredicateContext;
	public predicate(_p: number): PredicateContext;
	@RuleVersion(0)
	public predicate(_p?: number): PredicateContext {
		if (_p === undefined) {
			_p = 0;
		}

		let _parentctx: ParserRuleContext = this._ctx;
		let _parentState: number = this.state;
		let _localctx: PredicateContext = new PredicateContext(this._ctx, _parentState);
		let _prevctx: PredicateContext = _localctx;
		let _startState: number = 2;
		this.enterRecursionRule(_localctx, 2, SoleParser.RULE_predicate, _p);
		let _la: number;
		try {
			let _alt: number;
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 76;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input,3,this._ctx) ) {
			case 1:
				{
				this.state = 60;
				this.match(SoleParser.LPAREN);
				this.state = 61;
				this.predicate(0);
				this.state = 62;
				this.match(SoleParser.RPAREN);
				}
				break;

			case 2:
				{
				this.state = 64;
				this.match(SoleParser.BANG);
				this.state = 65;
				this.predicate(7);
				}
				break;

			case 3:
				{
				this.state = 66;
				this.expression(0);
				this.state = 67;
				_la = this._input.LA(1);
				if ( !(((((_la - 23)) & ~0x1F) === 0 && ((1 << (_la - 23)) & ((1 << (SoleParser.GT - 23)) | (1 << (SoleParser.LT - 23)) | (1 << (SoleParser.LE - 23)) | (1 << (SoleParser.GE - 23)))) !== 0)) ) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				this.state = 68;
				this.expression(0);
				}
				break;

			case 4:
				{
				this.state = 70;
				this.expression(0);
				this.state = 71;
				_la = this._input.LA(1);
				if ( !(((((_la - 9)) & ~0x1F) === 0 && ((1 << (_la - 9)) & ((1 << (SoleParser.LIKE - 9)) | (1 << (SoleParser.EQUAL - 9)) | (1 << (SoleParser.NOTEQUAL - 9)))) !== 0)) ) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				this.state = 72;
				this.expression(0);
				}
				break;

			case 5:
				{
				this.state = 74;
				this.functionCall();
				}
				break;

			case 6:
				{
				this.state = 75;
				this.match(SoleParser.Identifier);
				}
				break;
			}
			this._ctx._stop = this._input.tryLT(-1);
			this.state = 86;
			this._errHandler.sync(this);
			_alt = this.interpreter.adaptivePredict(this._input,5,this._ctx);
			while ( _alt!==2 && _alt!==ATN.INVALID_ALT_NUMBER ) {
				if ( _alt===1 ) {
					if ( this._parseListeners!=null ) this.triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					this.state = 84;
					this._errHandler.sync(this);
					switch ( this.interpreter.adaptivePredict(this._input,4,this._ctx) ) {
					case 1:
						{
						_localctx = new PredicateContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, SoleParser.RULE_predicate);
						this.state = 78;
						if (!(this.precpred(this._ctx, 6))) throw new FailedPredicateException(this, "this.precpred(this._ctx, 6)");
						this.state = 79;
						this.match(SoleParser.AND);
						this.state = 80;
						this.predicate(7);
						}
						break;

					case 2:
						{
						_localctx = new PredicateContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, SoleParser.RULE_predicate);
						this.state = 81;
						if (!(this.precpred(this._ctx, 5))) throw new FailedPredicateException(this, "this.precpred(this._ctx, 5)");
						this.state = 82;
						this.match(SoleParser.OR);
						this.state = 83;
						this.predicate(6);
						}
						break;
					}
					} 
				}
				this.state = 88;
				this._errHandler.sync(this);
				_alt = this.interpreter.adaptivePredict(this._input,5,this._ctx);
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}
	@RuleVersion(0)
	public functionCall(): FunctionCallContext {
		let _localctx: FunctionCallContext = new FunctionCallContext(this._ctx, this.state);
		this.enterRule(_localctx, 4, SoleParser.RULE_functionCall);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 89;
			this.match(SoleParser.Identifier);
			this.state = 90;
			this.match(SoleParser.LPAREN);
			this.state = 105;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if ((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << SoleParser.T__0) | (1 << SoleParser.Number) | (1 << SoleParser.StringLiteral) | (1 << SoleParser.LPAREN) | (1 << SoleParser.BANG))) !== 0) || _la===SoleParser.SUB || _la===SoleParser.Identifier) {
				{
				this.state = 93;
				this._errHandler.sync(this);
				switch ( this.interpreter.adaptivePredict(this._input,6,this._ctx) ) {
				case 1:
					{
					this.state = 91;
					this.expression(0);
					}
					break;

				case 2:
					{
					this.state = 92;
					this.predicate(0);
					}
					break;
				}
				this.state = 102;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				while (_la===SoleParser.COMMA) {
					{
					{
					this.state = 95;
					this.match(SoleParser.COMMA);
					this.state = 98;
					this._errHandler.sync(this);
					switch ( this.interpreter.adaptivePredict(this._input,7,this._ctx) ) {
					case 1:
						{
						this.state = 96;
						this.expression(0);
						}
						break;

					case 2:
						{
						this.state = 97;
						this.predicate(0);
						}
						break;
					}
					}
					}
					this.state = 104;
					this._errHandler.sync(this);
					_la = this._input.LA(1);
				}
				}
			}

			this.state = 107;
			this.match(SoleParser.RPAREN);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	@RuleVersion(0)
	public functionDef(): FunctionDefContext {
		let _localctx: FunctionDefContext = new FunctionDefContext(this._ctx, this.state);
		this.enterRule(_localctx, 6, SoleParser.RULE_functionDef);
		let _la: number;
		try {
			this.state = 141;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input,14,this._ctx) ) {
			case 1:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 109;
				this.match(SoleParser.DEF);
				this.state = 110;
				this.match(SoleParser.NATIVE);
				this.state = 111;
				this.match(SoleParser.Identifier);
				this.state = 112;
				this.match(SoleParser.LPAREN);
				this.state = 121;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (_la===SoleParser.Identifier) {
					{
					this.state = 113;
					this.match(SoleParser.Identifier);
					this.state = 118;
					this._errHandler.sync(this);
					_la = this._input.LA(1);
					while (_la===SoleParser.COMMA) {
						{
						{
						this.state = 114;
						this.match(SoleParser.COMMA);
						this.state = 115;
						this.match(SoleParser.Identifier);
						}
						}
						this.state = 120;
						this._errHandler.sync(this);
						_la = this._input.LA(1);
					}
					}
				}

				this.state = 123;
				this.match(SoleParser.RPAREN);
				this.state = 124;
				this.match(SoleParser.LBRACE);
				this.state = 125;
				this.match(SoleParser.RBRACE);
				}
				break;

			case 2:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 126;
				this.match(SoleParser.DEF);
				this.state = 127;
				this.match(SoleParser.Identifier);
				this.state = 128;
				this.match(SoleParser.LPAREN);
				this.state = 137;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (_la===SoleParser.Identifier) {
					{
					this.state = 129;
					this.match(SoleParser.Identifier);
					this.state = 134;
					this._errHandler.sync(this);
					_la = this._input.LA(1);
					while (_la===SoleParser.COMMA) {
						{
						{
						this.state = 130;
						this.match(SoleParser.COMMA);
						this.state = 131;
						this.match(SoleParser.Identifier);
						}
						}
						this.state = 136;
						this._errHandler.sync(this);
						_la = this._input.LA(1);
					}
					}
				}

				this.state = 139;
				this.match(SoleParser.RPAREN);
				this.state = 140;
				this.statementBlock();
				}
				break;
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	@RuleVersion(0)
	public globalsDef(): GlobalsDefContext {
		let _localctx: GlobalsDefContext = new GlobalsDefContext(this._ctx, this.state);
		this.enterRule(_localctx, 8, SoleParser.RULE_globalsDef);
		let _la: number;
		try {
			this.state = 154;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input,16,this._ctx) ) {
			case 1:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 143;
				this.match(SoleParser.DEF);
				this.state = 144;
				this.match(SoleParser.NATIVE);
				this.state = 145;
				this.match(SoleParser.Identifier);
				this.state = 146;
				this.match(SoleParser.SEMI);
				}
				break;

			case 2:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 147;
				this.match(SoleParser.DEF);
				this.state = 148;
				this.match(SoleParser.Identifier);
				this.state = 151;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (_la===SoleParser.ASSIGN) {
					{
					this.state = 149;
					this.match(SoleParser.ASSIGN);
					this.state = 150;
					_la = this._input.LA(1);
					if ( !(_la===SoleParser.Number || _la===SoleParser.StringLiteral) ) {
					this._errHandler.recoverInline(this);
					} else {
						if (this._input.LA(1) === Token.EOF) {
							this.matchedEOF = true;
						}

						this._errHandler.reportMatch(this);
						this.consume();
					}
					}
				}

				this.state = 153;
				this.match(SoleParser.SEMI);
				}
				break;
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	@RuleVersion(0)
	public statementBlock(): StatementBlockContext {
		let _localctx: StatementBlockContext = new StatementBlockContext(this._ctx, this.state);
		this.enterRule(_localctx, 10, SoleParser.RULE_statementBlock);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 156;
			this.match(SoleParser.LBRACE);
			this.state = 160;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			while ((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << SoleParser.IF) | (1 << SoleParser.DEF) | (1 << SoleParser.WHILE) | (1 << SoleParser.DO) | (1 << SoleParser.RETURN) | (1 << SoleParser.LBRACE) | (1 << SoleParser.SEMI))) !== 0) || _la===SoleParser.Identifier) {
				{
				{
				this.state = 157;
				this.statement();
				}
				}
				this.state = 162;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
			}
			this.state = 163;
			this.match(SoleParser.RBRACE);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	@RuleVersion(0)
	public statement(): StatementContext {
		let _localctx: StatementContext = new StatementContext(this._ctx, this.state);
		this.enterRule(_localctx, 12, SoleParser.RULE_statement);
		let _la: number;
		try {
			this.state = 214;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input,22,this._ctx) ) {
			case 1:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 165;
				this.statementBlock();
				}
				break;

			case 2:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 166;
				this.match(SoleParser.DEF);
				this.state = 167;
				this.match(SoleParser.Identifier);
				this.state = 173;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (_la===SoleParser.ASSIGN) {
					{
					this.state = 168;
					this.match(SoleParser.ASSIGN);
					this.state = 171;
					this._errHandler.sync(this);
					switch ( this.interpreter.adaptivePredict(this._input,18,this._ctx) ) {
					case 1:
						{
						this.state = 169;
						this.expression(0);
						}
						break;

					case 2:
						{
						this.state = 170;
						this.predicate(0);
						}
						break;
					}
					}
				}

				this.state = 175;
				this.match(SoleParser.SEMI);
				}
				break;

			case 3:
				this.enterOuterAlt(_localctx, 3);
				{
				this.state = 176;
				this.match(SoleParser.Identifier);
				this.state = 177;
				this.match(SoleParser.ASSIGN);
				this.state = 178;
				this.expression(0);
				this.state = 179;
				this.match(SoleParser.SEMI);
				}
				break;

			case 4:
				this.enterOuterAlt(_localctx, 4);
				{
				this.state = 181;
				this.functionCall();
				this.state = 182;
				this.match(SoleParser.SEMI);
				}
				break;

			case 5:
				this.enterOuterAlt(_localctx, 5);
				{
				this.state = 184;
				this.match(SoleParser.IF);
				this.state = 185;
				this.match(SoleParser.LPAREN);
				this.state = 186;
				this.predicate(0);
				this.state = 187;
				this.match(SoleParser.RPAREN);
				this.state = 188;
				this.statement();
				this.state = 191;
				this._errHandler.sync(this);
				switch ( this.interpreter.adaptivePredict(this._input,20,this._ctx) ) {
				case 1:
					{
					this.state = 189;
					this.match(SoleParser.ELSE);
					this.state = 190;
					this.statement();
					}
					break;
				}
				}
				break;

			case 6:
				this.enterOuterAlt(_localctx, 6);
				{
				this.state = 193;
				this.match(SoleParser.WHILE);
				this.state = 194;
				this.match(SoleParser.LPAREN);
				this.state = 195;
				this.predicate(0);
				this.state = 196;
				this.match(SoleParser.RPAREN);
				this.state = 197;
				this.statement();
				}
				break;

			case 7:
				this.enterOuterAlt(_localctx, 7);
				{
				this.state = 199;
				this.match(SoleParser.DO);
				this.state = 200;
				this.statement();
				this.state = 201;
				this.match(SoleParser.WHILE);
				this.state = 202;
				this.match(SoleParser.LPAREN);
				this.state = 203;
				this.predicate(0);
				this.state = 204;
				this.match(SoleParser.RPAREN);
				this.state = 205;
				this.match(SoleParser.SEMI);
				}
				break;

			case 8:
				this.enterOuterAlt(_localctx, 8);
				{
				this.state = 207;
				this.match(SoleParser.RETURN);
				this.state = 210;
				this._errHandler.sync(this);
				switch ( this.interpreter.adaptivePredict(this._input,21,this._ctx) ) {
				case 1:
					{
					this.state = 208;
					this.expression(0);
					}
					break;

				case 2:
					{
					this.state = 209;
					this.predicate(0);
					}
					break;
				}
				this.state = 212;
				this.match(SoleParser.SEMI);
				}
				break;

			case 9:
				this.enterOuterAlt(_localctx, 9);
				{
				this.state = 213;
				this.match(SoleParser.SEMI);
				}
				break;
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	@RuleVersion(0)
	public importDef(): ImportDefContext {
		let _localctx: ImportDefContext = new ImportDefContext(this._ctx, this.state);
		this.enterRule(_localctx, 14, SoleParser.RULE_importDef);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 216;
			this.match(SoleParser.IMPORT);
			this.state = 217;
			this.match(SoleParser.StringLiteral);
			this.state = 218;
			this.match(SoleParser.SEMI);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	@RuleVersion(0)
	public program(): ProgramContext {
		let _localctx: ProgramContext = new ProgramContext(this._ctx, this.state);
		this.enterRule(_localctx, 16, SoleParser.RULE_program);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 223;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			while (_la===SoleParser.IMPORT) {
				{
				{
				this.state = 220;
				this.importDef();
				}
				}
				this.state = 225;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
			}
			this.state = 228; 
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			do {
				{
				this.state = 228;
				this._errHandler.sync(this);
				switch ( this.interpreter.adaptivePredict(this._input,24,this._ctx) ) {
				case 1:
					{
					this.state = 226;
					this.globalsDef();
					}
					break;

				case 2:
					{
					this.state = 227;
					this.functionDef();
					}
					break;
				}
				}
				this.state = 230; 
				this._errHandler.sync(this);
				_la = this._input.LA(1);
			} while ( _la===SoleParser.DEF );
			this.state = 232;
			this.match(SoleParser.EOF);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}

	public sempred(_localctx: RuleContext, ruleIndex: number, predIndex: number): boolean {
		switch (ruleIndex) {
		case 0:
			return this.expression_sempred(_localctx as ExpressionContext, predIndex);

		case 1:
			return this.predicate_sempred(_localctx as PredicateContext, predIndex);
		}
		return true;
	}
	private expression_sempred(_localctx: ExpressionContext, predIndex: number): boolean {
		switch (predIndex) {
		case 0:
			return this.precpred(this._ctx, 9);

		case 1:
			return this.precpred(this._ctx, 8);

		case 2:
			return this.precpred(this._ctx, 6);

		case 3:
			return this.precpred(this._ctx, 5);
		}
		return true;
	}
	private predicate_sempred(_localctx: PredicateContext, predIndex: number): boolean {
		switch (predIndex) {
		case 4:
			return this.precpred(this._ctx, 6);

		case 5:
			return this.precpred(this._ctx, 5);
		}
		return true;
	}

	public static readonly _serializedATN: string =
		"\x03\uAF6F\u8320\u479D\uB75C\u4880\u1605\u191C\uAB37\x033\xED\x04\x02"+
		"\t\x02\x04\x03\t\x03\x04\x04\t\x04\x04\x05\t\x05\x04\x06\t\x06\x04\x07"+
		"\t\x07\x04\b\t\b\x04\t\t\t\x04\n\t\n\x03\x02\x03\x02\x03\x02\x03\x02\x03"+
		"\x02\x03\x02\x03\x02\x03\x02\x03\x02\x03\x02\x03\x02\x03\x02\x03\x02\x03"+
		"\x02\x03\x02\x03\x02\x03\x02\x03\x02\x03\x02\x03\x02\x05\x02)\n\x02\x03"+
		"\x02\x03\x02\x03\x02\x03\x02\x03\x02\x03\x02\x03\x02\x03\x02\x03\x02\x03"+
		"\x02\x03\x02\x03\x02\x03\x02\x03\x02\x07\x029\n\x02\f\x02\x0E\x02<\v\x02"+
		"\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03"+
		"\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x05\x03"+
		"O\n\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x07\x03W\n\x03"+
		"\f\x03\x0E\x03Z\v\x03\x03\x04\x03\x04\x03\x04\x03\x04\x05\x04`\n\x04\x03"+
		"\x04\x03\x04\x03\x04\x05\x04e\n\x04\x07\x04g\n\x04\f\x04\x0E\x04j\v\x04"+
		"\x05\x04l\n\x04\x03\x04\x03\x04\x03\x05\x03\x05\x03\x05\x03\x05\x03\x05"+
		"\x03\x05\x03\x05\x07\x05w\n\x05\f\x05\x0E\x05z\v\x05\x05\x05|\n\x05\x03"+
		"\x05\x03\x05\x03\x05\x03\x05\x03\x05\x03\x05\x03\x05\x03\x05\x03\x05\x07"+
		"\x05\x87\n\x05\f\x05\x0E\x05\x8A\v\x05\x05\x05\x8C\n\x05\x03\x05\x03\x05"+
		"\x05\x05\x90\n\x05\x03\x06\x03\x06\x03\x06\x03\x06\x03\x06\x03\x06\x03"+
		"\x06\x03\x06\x05\x06\x9A\n\x06\x03\x06\x05\x06\x9D\n\x06\x03\x07\x03\x07"+
		"\x07\x07\xA1\n\x07\f\x07\x0E\x07\xA4\v\x07\x03\x07\x03\x07\x03\b\x03\b"+
		"\x03\b\x03\b\x03\b\x03\b\x05\b\xAE\n\b\x05\b\xB0\n\b\x03\b\x03\b\x03\b"+
		"\x03\b\x03\b\x03\b\x03\b\x03\b\x03\b\x03\b\x03\b\x03\b\x03\b\x03\b\x03"+
		"\b\x03\b\x05\b\xC2\n\b\x03\b\x03\b\x03\b\x03\b\x03\b\x03\b\x03\b\x03\b"+
		"\x03\b\x03\b\x03\b\x03\b\x03\b\x03\b\x03\b\x03\b\x03\b\x05\b\xD5\n\b\x03"+
		"\b\x03\b\x05\b\xD9\n\b\x03\t\x03\t\x03\t\x03\t\x03\n\x07\n\xE0\n\n\f\n"+
		"\x0E\n\xE3\v\n\x03\n\x03\n\x06\n\xE7\n\n\r\n\x0E\n\xE8\x03\n\x03\n\x03"+
		"\n\x02\x02\x04\x02\x04\v\x02\x02\x04\x02\x06\x02\b\x02\n\x02\f\x02\x0E"+
		"\x02\x10\x02\x12\x02\x02\x07\x04\x02*+//\x03\x02()\x04\x02\x19\x1A!\""+
		"\x05\x02\v\v  ##\x03\x02\x04\x05\u0110\x02(\x03\x02\x02\x02\x04N\x03\x02"+
		"\x02\x02\x06[\x03\x02\x02\x02\b\x8F\x03\x02\x02\x02\n\x9C\x03\x02\x02"+
		"\x02\f\x9E\x03\x02\x02\x02\x0E\xD8\x03\x02\x02\x02\x10\xDA\x03\x02\x02"+
		"\x02\x12\xE1\x03\x02\x02\x02\x14\x15\b\x02\x01\x02\x15\x16\x07\x0F\x02"+
		"\x02\x16\x17\x05\x02\x02\x02\x17\x18\x07\x10\x02\x02\x18)\x03\x02\x02"+
		"\x02\x19\x1A\x07)\x02\x02\x1A)\x05\x02\x02\f\x1B\x1C\x07\x03\x02\x02\x1C"+
		"\x1D\x07\x0F\x02\x02\x1D\x1E\x05\x04\x03\x02\x1E\x1F\x07\x16\x02\x02\x1F"+
		" \x05\x02\x02\x02 !\x07\x16\x02\x02!\"\x05\x02\x02\x02\"#\x07\x10\x02"+
		"\x02#)\x03\x02\x02\x02$)\x05\x06\x04\x02%)\x073\x02\x02&)\x07\x04\x02"+
		"\x02\')\x07\x05\x02\x02(\x14\x03\x02\x02\x02(\x19\x03\x02\x02\x02(\x1B"+
		"\x03\x02\x02\x02($\x03\x02\x02\x02(%\x03\x02\x02\x02(&\x03\x02\x02\x02"+
		"(\'\x03\x02\x02\x02):\x03\x02\x02\x02*+\f\v\x02\x02+,\t\x02\x02\x02,9"+
		"\x05\x02\x02\f-.\f\n\x02\x02./\t\x03\x02\x02/9\x05\x02\x02\v01\f\b\x02"+
		"\x0212\x07\x1F\x02\x0229\x05\x02\x02\t34\f\x07\x02\x0245\x07\x13\x02\x02"+
		"56\x05\x02\x02\x0267\x07\x14\x02\x0279\x03\x02\x02\x028*\x03\x02\x02\x02"+
		"8-\x03\x02\x02\x0280\x03\x02\x02\x0283\x03\x02\x02\x029<\x03\x02\x02\x02"+
		":8\x03\x02\x02\x02:;\x03\x02\x02\x02;\x03\x03\x02\x02\x02<:\x03\x02\x02"+
		"\x02=>\b\x03\x01\x02>?\x07\x0F\x02\x02?@\x05\x04\x03\x02@A\x07\x10\x02"+
		"\x02AO\x03\x02\x02\x02BC\x07\x1B\x02\x02CO\x05\x04\x03\tDE\x05\x02\x02"+
		"\x02EF\t\x04\x02\x02FG\x05\x02\x02\x02GO\x03\x02\x02\x02HI\x05\x02\x02"+
		"\x02IJ\t\x05\x02\x02JK\x05\x02\x02\x02KO\x03\x02\x02\x02LO\x05\x06\x04"+
		"\x02MO\x073\x02\x02N=\x03\x02\x02\x02NB\x03\x02\x02\x02ND\x03\x02\x02"+
		"\x02NH\x03\x02\x02\x02NL\x03\x02\x02\x02NM\x03\x02\x02\x02OX\x03\x02\x02"+
		"\x02PQ\f\b\x02\x02QR\x07$\x02\x02RW\x05\x04\x03\tST\f\x07\x02\x02TU\x07"+
		"%\x02\x02UW\x05\x04\x03\bVP\x03\x02\x02\x02VS\x03\x02\x02\x02WZ\x03\x02"+
		"\x02\x02XV\x03\x02\x02\x02XY\x03\x02\x02\x02Y\x05\x03\x02\x02\x02ZX\x03"+
		"\x02\x02\x02[\\\x073\x02\x02\\k\x07\x0F\x02\x02]`\x05\x02\x02\x02^`\x05"+
		"\x04\x03\x02_]\x03\x02\x02\x02_^\x03\x02\x02\x02`h\x03\x02\x02\x02ad\x07"+
		"\x16\x02\x02be\x05\x02\x02\x02ce\x05\x04\x03\x02db\x03\x02\x02\x02dc\x03"+
		"\x02\x02\x02eg\x03\x02\x02\x02fa\x03\x02\x02\x02gj\x03\x02\x02\x02hf\x03"+
		"\x02\x02\x02hi\x03\x02\x02\x02il\x03\x02\x02\x02jh\x03\x02\x02\x02k_\x03"+
		"\x02\x02\x02kl\x03\x02\x02\x02lm\x03\x02\x02\x02mn\x07\x10\x02\x02n\x07"+
		"\x03\x02\x02\x02op\x07\x07\x02\x02pq\x07\x0E\x02\x02qr\x073\x02\x02r{"+
		"\x07\x0F\x02\x02sx\x073\x02\x02tu\x07\x16\x02\x02uw\x073\x02\x02vt\x03"+
		"\x02\x02\x02wz\x03\x02\x02\x02xv\x03\x02\x02\x02xy\x03\x02\x02\x02y|\x03"+
		"\x02\x02\x02zx\x03\x02\x02\x02{s\x03\x02\x02\x02{|\x03\x02\x02\x02|}\x03"+
		"\x02\x02\x02}~\x07\x10\x02\x02~\x7F\x07\x11\x02\x02\x7F\x90\x07\x12\x02"+
		"\x02\x80\x81\x07\x07\x02\x02\x81\x82\x073\x02\x02\x82\x8B\x07\x0F\x02"+
		"\x02\x83\x88\x073\x02\x02\x84\x85\x07\x16\x02\x02\x85\x87\x073\x02\x02"+
		"\x86\x84\x03\x02\x02\x02\x87\x8A\x03\x02\x02\x02\x88\x86\x03\x02\x02\x02"+
		"\x88\x89\x03\x02\x02\x02\x89\x8C\x03\x02\x02\x02\x8A\x88\x03\x02\x02\x02"+
		"\x8B\x83\x03\x02\x02\x02\x8B\x8C\x03\x02\x02\x02\x8C\x8D\x03\x02\x02\x02"+
		"\x8D\x8E\x07\x10\x02\x02\x8E\x90\x05\f\x07\x02\x8Fo\x03\x02\x02\x02\x8F"+
		"\x80\x03\x02\x02\x02\x90\t\x03\x02\x02\x02\x91\x92\x07\x07\x02\x02\x92"+
		"\x93\x07\x0E\x02\x02\x93\x94\x073\x02\x02\x94\x9D\x07\x15\x02\x02\x95"+
		"\x96\x07\x07\x02\x02\x96\x99\x073\x02\x02\x97\x98\x07\x18\x02\x02\x98"+
		"\x9A\t\x06\x02\x02\x99\x97\x03\x02\x02\x02\x99\x9A\x03\x02\x02\x02\x9A"+
		"\x9B\x03\x02\x02\x02\x9B\x9D\x07\x15\x02\x02\x9C\x91\x03\x02\x02\x02\x9C"+
		"\x95\x03\x02\x02\x02\x9D\v\x03\x02\x02\x02\x9E\xA2\x07\x11\x02\x02\x9F"+
		"\xA1\x05\x0E\b\x02\xA0\x9F\x03\x02\x02\x02\xA1\xA4\x03\x02\x02\x02\xA2"+
		"\xA0\x03\x02\x02\x02\xA2\xA3\x03\x02\x02\x02\xA3\xA5\x03\x02\x02\x02\xA4"+
		"\xA2\x03\x02\x02\x02\xA5\xA6\x07\x12\x02\x02\xA6\r\x03\x02\x02\x02\xA7"+
		"\xD9\x05\f\x07\x02\xA8\xA9\x07\x07\x02\x02\xA9\xAF\x073\x02\x02\xAA\xAD"+
		"\x07\x18\x02\x02\xAB\xAE\x05\x02\x02\x02\xAC\xAE\x05\x04\x03\x02\xAD\xAB"+
		"\x03\x02\x02\x02\xAD\xAC\x03\x02\x02\x02\xAE\xB0\x03\x02\x02\x02\xAF\xAA"+
		"\x03\x02\x02\x02\xAF\xB0\x03\x02\x02\x02\xB0\xB1\x03\x02\x02\x02\xB1\xD9"+
		"\x07\x15\x02\x02\xB2\xB3\x073\x02\x02\xB3\xB4\x07\x18\x02\x02\xB4\xB5"+
		"\x05\x02\x02\x02\xB5\xB6\x07\x15\x02\x02\xB6\xD9\x03\x02\x02\x02\xB7\xB8"+
		"\x05\x06\x04\x02\xB8\xB9\x07\x15\x02\x02\xB9\xD9\x03\x02\x02\x02\xBA\xBB"+
		"\x07\x06\x02\x02\xBB\xBC\x07\x0F\x02\x02\xBC\xBD\x05\x04\x03\x02\xBD\xBE"+
		"\x07\x10\x02\x02\xBE\xC1\x05\x0E\b\x02\xBF\xC0\x07\b\x02\x02\xC0\xC2\x05"+
		"\x0E\b\x02\xC1\xBF\x03\x02\x02\x02\xC1\xC2\x03\x02\x02\x02\xC2\xD9\x03"+
		"\x02\x02\x02\xC3\xC4\x07\t\x02\x02\xC4\xC5\x07\x0F\x02\x02\xC5\xC6\x05"+
		"\x04\x03\x02\xC6\xC7\x07\x10\x02\x02\xC7\xC8\x05\x0E\b\x02\xC8\xD9\x03"+
		"\x02\x02\x02\xC9\xCA\x07\n\x02\x02\xCA\xCB\x05\x0E\b\x02\xCB\xCC\x07\t"+
		"\x02\x02\xCC\xCD\x07\x0F\x02\x02\xCD\xCE\x05\x04\x03\x02\xCE\xCF\x07\x10"+
		"\x02\x02\xCF\xD0\x07\x15\x02\x02\xD0\xD9\x03\x02\x02\x02\xD1\xD4\x07\f"+
		"\x02\x02\xD2\xD5\x05\x02\x02\x02\xD3\xD5\x05\x04\x03\x02\xD4\xD2\x03\x02"+
		"\x02\x02\xD4\xD3\x03\x02\x02\x02\xD4\xD5\x03\x02\x02\x02\xD5\xD6\x03\x02"+
		"\x02\x02\xD6\xD9\x07\x15\x02\x02\xD7\xD9\x07\x15\x02\x02\xD8\xA7\x03\x02"+
		"\x02\x02\xD8\xA8\x03\x02\x02\x02\xD8\xB2\x03\x02\x02\x02\xD8\xB7\x03\x02"+
		"\x02\x02\xD8\xBA\x03\x02\x02\x02\xD8\xC3\x03\x02\x02\x02\xD8\xC9\x03\x02"+
		"\x02\x02\xD8\xD1\x03\x02\x02\x02\xD8\xD7\x03\x02\x02\x02\xD9\x0F\x03\x02"+
		"\x02\x02\xDA\xDB\x07\r\x02\x02\xDB\xDC\x07\x05\x02\x02\xDC\xDD\x07\x15"+
		"\x02\x02\xDD\x11\x03\x02\x02\x02\xDE\xE0\x05\x10\t\x02\xDF\xDE\x03\x02"+
		"\x02\x02\xE0\xE3\x03\x02\x02\x02\xE1\xDF\x03\x02\x02\x02\xE1\xE2\x03\x02"+
		"\x02\x02\xE2\xE6\x03\x02\x02\x02\xE3\xE1\x03\x02\x02\x02\xE4\xE7\x05\n"+
		"\x06\x02\xE5\xE7\x05\b\x05\x02\xE6\xE4\x03\x02\x02\x02\xE6\xE5\x03\x02"+
		"\x02\x02\xE7\xE8\x03\x02\x02\x02\xE8\xE6\x03\x02\x02\x02\xE8\xE9\x03\x02"+
		"\x02\x02\xE9\xEA\x03\x02\x02\x02\xEA\xEB\x07\x02\x02\x03\xEB\x13\x03\x02"+
		"\x02\x02\x1C(8:NVX_dhkx{\x88\x8B\x8F\x99\x9C\xA2\xAD\xAF\xC1\xD4\xD8\xE1"+
		"\xE6\xE8";
	public static __ATN: ATN;
	public static get _ATN(): ATN {
		if (!SoleParser.__ATN) {
			SoleParser.__ATN = new ATNDeserializer().deserialize(Utils.toCharArray(SoleParser._serializedATN));
		}

		return SoleParser.__ATN;
	}

}

export class ExpressionContext extends ParserRuleContext {
	public expression(): ExpressionContext[];
	public expression(i: number): ExpressionContext;
	public expression(i?: number): ExpressionContext | ExpressionContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ExpressionContext);
		} else {
			return this.getRuleContext(i, ExpressionContext);
		}
	}
	public predicate(): PredicateContext | undefined {
		return this.tryGetRuleContext(0, PredicateContext);
	}
	public functionCall(): FunctionCallContext | undefined {
		return this.tryGetRuleContext(0, FunctionCallContext);
	}
	public Identifier(): TerminalNode | undefined { return this.tryGetToken(SoleParser.Identifier, 0); }
	public Number(): TerminalNode | undefined { return this.tryGetToken(SoleParser.Number, 0); }
	public StringLiteral(): TerminalNode | undefined { return this.tryGetToken(SoleParser.StringLiteral, 0); }
	constructor(parent: ParserRuleContext, invokingState: number);
	constructor(parent: ParserRuleContext, invokingState: number) {
		super(parent, invokingState);

	}
	@Override public get ruleIndex(): number { return SoleParser.RULE_expression; }
	@Override
	public enterRule(listener: SoleListener): void {
		if (listener.enterExpression) listener.enterExpression(this);
	}
	@Override
	public exitRule(listener: SoleListener): void {
		if (listener.exitExpression) listener.exitExpression(this);
	}
	@Override
	public accept<Result>(visitor: SoleVisitor<Result>): Result {
		if (visitor.visitExpression) return visitor.visitExpression(this);
		else return visitor.visitChildren(this);
	}
}


export class PredicateContext extends ParserRuleContext {
	public predicate(): PredicateContext[];
	public predicate(i: number): PredicateContext;
	public predicate(i?: number): PredicateContext | PredicateContext[] {
		if (i === undefined) {
			return this.getRuleContexts(PredicateContext);
		} else {
			return this.getRuleContext(i, PredicateContext);
		}
	}
	public expression(): ExpressionContext[];
	public expression(i: number): ExpressionContext;
	public expression(i?: number): ExpressionContext | ExpressionContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ExpressionContext);
		} else {
			return this.getRuleContext(i, ExpressionContext);
		}
	}
	public functionCall(): FunctionCallContext | undefined {
		return this.tryGetRuleContext(0, FunctionCallContext);
	}
	public Identifier(): TerminalNode | undefined { return this.tryGetToken(SoleParser.Identifier, 0); }
	constructor(parent: ParserRuleContext, invokingState: number);
	constructor(parent: ParserRuleContext, invokingState: number) {
		super(parent, invokingState);

	}
	@Override public get ruleIndex(): number { return SoleParser.RULE_predicate; }
	@Override
	public enterRule(listener: SoleListener): void {
		if (listener.enterPredicate) listener.enterPredicate(this);
	}
	@Override
	public exitRule(listener: SoleListener): void {
		if (listener.exitPredicate) listener.exitPredicate(this);
	}
	@Override
	public accept<Result>(visitor: SoleVisitor<Result>): Result {
		if (visitor.visitPredicate) return visitor.visitPredicate(this);
		else return visitor.visitChildren(this);
	}
}


export class FunctionCallContext extends ParserRuleContext {
	public Identifier(): TerminalNode { return this.getToken(SoleParser.Identifier, 0); }
	public expression(): ExpressionContext[];
	public expression(i: number): ExpressionContext;
	public expression(i?: number): ExpressionContext | ExpressionContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ExpressionContext);
		} else {
			return this.getRuleContext(i, ExpressionContext);
		}
	}
	public predicate(): PredicateContext[];
	public predicate(i: number): PredicateContext;
	public predicate(i?: number): PredicateContext | PredicateContext[] {
		if (i === undefined) {
			return this.getRuleContexts(PredicateContext);
		} else {
			return this.getRuleContext(i, PredicateContext);
		}
	}
	constructor(parent: ParserRuleContext, invokingState: number);
	constructor(parent: ParserRuleContext, invokingState: number) {
		super(parent, invokingState);

	}
	@Override public get ruleIndex(): number { return SoleParser.RULE_functionCall; }
	@Override
	public enterRule(listener: SoleListener): void {
		if (listener.enterFunctionCall) listener.enterFunctionCall(this);
	}
	@Override
	public exitRule(listener: SoleListener): void {
		if (listener.exitFunctionCall) listener.exitFunctionCall(this);
	}
	@Override
	public accept<Result>(visitor: SoleVisitor<Result>): Result {
		if (visitor.visitFunctionCall) return visitor.visitFunctionCall(this);
		else return visitor.visitChildren(this);
	}
}


export class FunctionDefContext extends ParserRuleContext {
	public Identifier(): TerminalNode[];
	public Identifier(i: number): TerminalNode;
	public Identifier(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(SoleParser.Identifier);
		} else {
			return this.getToken(SoleParser.Identifier, i);
		}
	}
	public statementBlock(): StatementBlockContext | undefined {
		return this.tryGetRuleContext(0, StatementBlockContext);
	}
	constructor(parent: ParserRuleContext, invokingState: number);
	constructor(parent: ParserRuleContext, invokingState: number) {
		super(parent, invokingState);

	}
	@Override public get ruleIndex(): number { return SoleParser.RULE_functionDef; }
	@Override
	public enterRule(listener: SoleListener): void {
		if (listener.enterFunctionDef) listener.enterFunctionDef(this);
	}
	@Override
	public exitRule(listener: SoleListener): void {
		if (listener.exitFunctionDef) listener.exitFunctionDef(this);
	}
	@Override
	public accept<Result>(visitor: SoleVisitor<Result>): Result {
		if (visitor.visitFunctionDef) return visitor.visitFunctionDef(this);
		else return visitor.visitChildren(this);
	}
}


export class GlobalsDefContext extends ParserRuleContext {
	public Identifier(): TerminalNode { return this.getToken(SoleParser.Identifier, 0); }
	public Number(): TerminalNode | undefined { return this.tryGetToken(SoleParser.Number, 0); }
	public StringLiteral(): TerminalNode | undefined { return this.tryGetToken(SoleParser.StringLiteral, 0); }
	constructor(parent: ParserRuleContext, invokingState: number);
	constructor(parent: ParserRuleContext, invokingState: number) {
		super(parent, invokingState);

	}
	@Override public get ruleIndex(): number { return SoleParser.RULE_globalsDef; }
	@Override
	public enterRule(listener: SoleListener): void {
		if (listener.enterGlobalsDef) listener.enterGlobalsDef(this);
	}
	@Override
	public exitRule(listener: SoleListener): void {
		if (listener.exitGlobalsDef) listener.exitGlobalsDef(this);
	}
	@Override
	public accept<Result>(visitor: SoleVisitor<Result>): Result {
		if (visitor.visitGlobalsDef) return visitor.visitGlobalsDef(this);
		else return visitor.visitChildren(this);
	}
}


export class StatementBlockContext extends ParserRuleContext {
	public statement(): StatementContext[];
	public statement(i: number): StatementContext;
	public statement(i?: number): StatementContext | StatementContext[] {
		if (i === undefined) {
			return this.getRuleContexts(StatementContext);
		} else {
			return this.getRuleContext(i, StatementContext);
		}
	}
	constructor(parent: ParserRuleContext, invokingState: number);
	constructor(parent: ParserRuleContext, invokingState: number) {
		super(parent, invokingState);

	}
	@Override public get ruleIndex(): number { return SoleParser.RULE_statementBlock; }
	@Override
	public enterRule(listener: SoleListener): void {
		if (listener.enterStatementBlock) listener.enterStatementBlock(this);
	}
	@Override
	public exitRule(listener: SoleListener): void {
		if (listener.exitStatementBlock) listener.exitStatementBlock(this);
	}
	@Override
	public accept<Result>(visitor: SoleVisitor<Result>): Result {
		if (visitor.visitStatementBlock) return visitor.visitStatementBlock(this);
		else return visitor.visitChildren(this);
	}
}


export class StatementContext extends ParserRuleContext {
	public statementBlock(): StatementBlockContext | undefined {
		return this.tryGetRuleContext(0, StatementBlockContext);
	}
	public Identifier(): TerminalNode | undefined { return this.tryGetToken(SoleParser.Identifier, 0); }
	public expression(): ExpressionContext | undefined {
		return this.tryGetRuleContext(0, ExpressionContext);
	}
	public predicate(): PredicateContext | undefined {
		return this.tryGetRuleContext(0, PredicateContext);
	}
	public functionCall(): FunctionCallContext | undefined {
		return this.tryGetRuleContext(0, FunctionCallContext);
	}
	public statement(): StatementContext[];
	public statement(i: number): StatementContext;
	public statement(i?: number): StatementContext | StatementContext[] {
		if (i === undefined) {
			return this.getRuleContexts(StatementContext);
		} else {
			return this.getRuleContext(i, StatementContext);
		}
	}
	constructor(parent: ParserRuleContext, invokingState: number);
	constructor(parent: ParserRuleContext, invokingState: number) {
		super(parent, invokingState);

	}
	@Override public get ruleIndex(): number { return SoleParser.RULE_statement; }
	@Override
	public enterRule(listener: SoleListener): void {
		if (listener.enterStatement) listener.enterStatement(this);
	}
	@Override
	public exitRule(listener: SoleListener): void {
		if (listener.exitStatement) listener.exitStatement(this);
	}
	@Override
	public accept<Result>(visitor: SoleVisitor<Result>): Result {
		if (visitor.visitStatement) return visitor.visitStatement(this);
		else return visitor.visitChildren(this);
	}
}


export class ImportDefContext extends ParserRuleContext {
	public StringLiteral(): TerminalNode { return this.getToken(SoleParser.StringLiteral, 0); }
	constructor(parent: ParserRuleContext, invokingState: number);
	constructor(parent: ParserRuleContext, invokingState: number) {
		super(parent, invokingState);

	}
	@Override public get ruleIndex(): number { return SoleParser.RULE_importDef; }
	@Override
	public enterRule(listener: SoleListener): void {
		if (listener.enterImportDef) listener.enterImportDef(this);
	}
	@Override
	public exitRule(listener: SoleListener): void {
		if (listener.exitImportDef) listener.exitImportDef(this);
	}
	@Override
	public accept<Result>(visitor: SoleVisitor<Result>): Result {
		if (visitor.visitImportDef) return visitor.visitImportDef(this);
		else return visitor.visitChildren(this);
	}
}


export class ProgramContext extends ParserRuleContext {
	public EOF(): TerminalNode { return this.getToken(SoleParser.EOF, 0); }
	public importDef(): ImportDefContext[];
	public importDef(i: number): ImportDefContext;
	public importDef(i?: number): ImportDefContext | ImportDefContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ImportDefContext);
		} else {
			return this.getRuleContext(i, ImportDefContext);
		}
	}
	public globalsDef(): GlobalsDefContext[];
	public globalsDef(i: number): GlobalsDefContext;
	public globalsDef(i?: number): GlobalsDefContext | GlobalsDefContext[] {
		if (i === undefined) {
			return this.getRuleContexts(GlobalsDefContext);
		} else {
			return this.getRuleContext(i, GlobalsDefContext);
		}
	}
	public functionDef(): FunctionDefContext[];
	public functionDef(i: number): FunctionDefContext;
	public functionDef(i?: number): FunctionDefContext | FunctionDefContext[] {
		if (i === undefined) {
			return this.getRuleContexts(FunctionDefContext);
		} else {
			return this.getRuleContext(i, FunctionDefContext);
		}
	}
	constructor(parent: ParserRuleContext, invokingState: number);
	constructor(parent: ParserRuleContext, invokingState: number) {
		super(parent, invokingState);

	}
	@Override public get ruleIndex(): number { return SoleParser.RULE_program; }
	@Override
	public enterRule(listener: SoleListener): void {
		if (listener.enterProgram) listener.enterProgram(this);
	}
	@Override
	public exitRule(listener: SoleListener): void {
		if (listener.exitProgram) listener.exitProgram(this);
	}
	@Override
	public accept<Result>(visitor: SoleVisitor<Result>): Result {
		if (visitor.visitProgram) return visitor.visitProgram(this);
		else return visitor.visitChildren(this);
	}
}


