// Generated from ./src/sole/Sole.g4 by ANTLR 4.6-SNAPSHOT


import { ParseTreeListener } from 'antlr4ts/tree/ParseTreeListener';

import { ExpressionContext } from './SoleParser';
import { PredicateContext } from './SoleParser';
import { FunctionCallContext } from './SoleParser';
import { FunctionDefContext } from './SoleParser';
import { GlobalsDefContext } from './SoleParser';
import { StatementBlockContext } from './SoleParser';
import { StatementContext } from './SoleParser';
import { ImportDefContext } from './SoleParser';
import { ProgramContext } from './SoleParser';


/**
 * This interface defines a complete listener for a parse tree produced by
 * `SoleParser`.
 */
export interface SoleListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by `SoleParser.expression`.
	 * @param ctx the parse tree
	 */
	enterExpression?: (ctx: ExpressionContext) => void;
	/**
	 * Exit a parse tree produced by `SoleParser.expression`.
	 * @param ctx the parse tree
	 */
	exitExpression?: (ctx: ExpressionContext) => void;

	/**
	 * Enter a parse tree produced by `SoleParser.predicate`.
	 * @param ctx the parse tree
	 */
	enterPredicate?: (ctx: PredicateContext) => void;
	/**
	 * Exit a parse tree produced by `SoleParser.predicate`.
	 * @param ctx the parse tree
	 */
	exitPredicate?: (ctx: PredicateContext) => void;

	/**
	 * Enter a parse tree produced by `SoleParser.functionCall`.
	 * @param ctx the parse tree
	 */
	enterFunctionCall?: (ctx: FunctionCallContext) => void;
	/**
	 * Exit a parse tree produced by `SoleParser.functionCall`.
	 * @param ctx the parse tree
	 */
	exitFunctionCall?: (ctx: FunctionCallContext) => void;

	/**
	 * Enter a parse tree produced by `SoleParser.functionDef`.
	 * @param ctx the parse tree
	 */
	enterFunctionDef?: (ctx: FunctionDefContext) => void;
	/**
	 * Exit a parse tree produced by `SoleParser.functionDef`.
	 * @param ctx the parse tree
	 */
	exitFunctionDef?: (ctx: FunctionDefContext) => void;

	/**
	 * Enter a parse tree produced by `SoleParser.globalsDef`.
	 * @param ctx the parse tree
	 */
	enterGlobalsDef?: (ctx: GlobalsDefContext) => void;
	/**
	 * Exit a parse tree produced by `SoleParser.globalsDef`.
	 * @param ctx the parse tree
	 */
	exitGlobalsDef?: (ctx: GlobalsDefContext) => void;

	/**
	 * Enter a parse tree produced by `SoleParser.statementBlock`.
	 * @param ctx the parse tree
	 */
	enterStatementBlock?: (ctx: StatementBlockContext) => void;
	/**
	 * Exit a parse tree produced by `SoleParser.statementBlock`.
	 * @param ctx the parse tree
	 */
	exitStatementBlock?: (ctx: StatementBlockContext) => void;

	/**
	 * Enter a parse tree produced by `SoleParser.statement`.
	 * @param ctx the parse tree
	 */
	enterStatement?: (ctx: StatementContext) => void;
	/**
	 * Exit a parse tree produced by `SoleParser.statement`.
	 * @param ctx the parse tree
	 */
	exitStatement?: (ctx: StatementContext) => void;

	/**
	 * Enter a parse tree produced by `SoleParser.importDef`.
	 * @param ctx the parse tree
	 */
	enterImportDef?: (ctx: ImportDefContext) => void;
	/**
	 * Exit a parse tree produced by `SoleParser.importDef`.
	 * @param ctx the parse tree
	 */
	exitImportDef?: (ctx: ImportDefContext) => void;

	/**
	 * Enter a parse tree produced by `SoleParser.program`.
	 * @param ctx the parse tree
	 */
	enterProgram?: (ctx: ProgramContext) => void;
	/**
	 * Exit a parse tree produced by `SoleParser.program`.
	 * @param ctx the parse tree
	 */
	exitProgram?: (ctx: ProgramContext) => void;
}

