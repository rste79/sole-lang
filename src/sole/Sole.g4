grammar Sole;

options {
  language = JavaScript;
}

expression
    : '(' expression ')'
    | '-' expression
    | expression ('*'|'/'|'%') expression
    | expression ('+'|'-') expression
    | 'iff' '(' predicate ',' expression ',' expression ')'
    | expression '?:' expression
    | expression '[' expression ']'
    | functionCall
    | Identifier
    | Number
    | StringLiteral
    ;

predicate
    : '(' predicate ')'
    | '!' predicate
    | predicate '&&' predicate
    | predicate '||' predicate
    | expression ('<=' | '>=' | '>' | '<') expression
    | expression ('==' | '!=' | 'like') expression
    | functionCall
    | Identifier 
    ;

functionCall
    : Identifier '(' ((expression | predicate) (',' (expression | predicate))*)? ')'
    ;

functionDef
    : 'def' 'native' Identifier '(' (Identifier (',' Identifier)*)? ')' '{' '}'
    | 'def' Identifier '(' (Identifier (',' Identifier)*)? ')' statementBlock
    ;

globalsDef
    : 'def' 'native' Identifier ';'
    | 'def' Identifier ('=' (Number | StringLiteral))? ';'
    ;

statementBlock
    : '{' statement* '}'
    ;

statement
    : statementBlock
    | 'def' Identifier ('=' (expression | predicate))? ';'
    | Identifier '=' expression ';'
    | functionCall ';'
    | 'if' '(' predicate ')' statement ('else' statement)?
    //| 'for' '(' forControl ')' statement
    | 'while' '(' predicate ')' statement
    | 'do' statement 'while' '(' predicate ')' ';'
    //| 'switch' '(' expression ')' '{' switchBlockStatementGroup* switchLabel* '}'
    | 'return' (expression | predicate)? ';'
    | ';'
    ;

importDef
    : 'import' StringLiteral ';'
    ;

program
    : importDef* (globalsDef | functionDef)+ EOF
    ;

// Lexer

Number
    : [0-9]+ ('.' [0-9])*
    ;

StringLiteral
: '"' StringCharacters? '"'
;

fragment
StringCharacters
: StringCharacter+
;

fragment
StringCharacter
: ~["\\]
| EscapeSequence
;

// ï¿½3.10.6 Escape Sequences for Character and String Literals

fragment
EscapeSequence
: '\\' [btnfr"'\\]
| OctalEscape
| UnicodeEscape
;

fragment
HexDigit
: [0-9a-fA-F]
;

fragment
OctalDigit
: [0-7]
;

fragment
OctalEscape
: '\\' OctalDigit
| '\\' OctalDigit OctalDigit
| '\\' ZeroToThree OctalDigit OctalDigit
;

fragment
UnicodeEscape
: '\\' 'u' HexDigit HexDigit HexDigit HexDigit
;

fragment
ZeroToThree
: [0-3]
;

IF : 'if';
DEF : 'def';
ELSE : 'else';
WHILE : 'while';
DO : 'do';
LIKE : 'like';
RETURN : 'return';
IMPORT : 'import';
NATIVE : 'native';

LPAREN : '(';
RPAREN : ')';
LBRACE : '{';
RBRACE : '}';
LBRACK : '[';
RBRACK : ']';
SEMI : ';';
COMMA : ',';
DOT : '.';

ASSIGN : '=';
GT : '>';
LT : '<';
BANG : '!';
TILDE : '~';
QUESTION : '?';
COLON : ':';
ELVIS : '?:';
EQUAL : '==';
LE : '<=';
GE : '>=';
NOTEQUAL : '!=';
AND : '&&';
OR : '||';
INC : '++';
DEC : '--';
ADD : '+';
SUB : '-';
MUL : '*';
DIV : '/';
BITAND : '&';
BITOR : '|';
CARET : '^';
MOD : '%';

WS : [ \t\r\n\u000C]+ -> skip
;

COMMENT
: '/*' .*? '*/' -> skip
;

LINE_COMMENT
: '//' ~[\r\n]* -> skip
;

Identifier
: [a-zA-Z_] [a-zA-Z_0-9]*
;
