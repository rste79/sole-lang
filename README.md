# sole-lang
The interpreter of Sole language for TypeScript

## Installation 
```sh
npm install sole-lang --save
```
## Usage
```typescript
import {SoleBuilder, Binding} from 'sole-lang'

let sb = new SoleBuilder();
let binding = new Binding(null);
let prog = sb.build(binding, 'def a(b, c) { return b + c; }');
let result = prog.call('a', [1,2])
console.log(result)
```
```sh
Result should be 3
```
## Test 
```sh
npm run test
```